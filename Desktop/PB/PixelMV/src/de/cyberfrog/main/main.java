package de.cyberfrog.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import de.cyberfrog.commands.c_pmvc;
import de.cyberfrog.commands.c_pmvl;
import de.cyberfrog.commands.c_pmvt;
import de.cyberfrog.listerner.l_click;

/**
 * 
 * Created by Prisemut on 20.03.2018
 */
public class main extends JavaPlugin {

	public static main instance;
	public static String pr = "�7[�4PixelMV�7] ";

	@Override
	public void onEnable() {
		System.out.println("[PixelMV] Loaded!");
		instance = this;

		this.getCommand("pmvl").setExecutor(new c_pmvl());
		this.getCommand("pmvt").setExecutor(new c_pmvt());
		this.getCommand("pmvc").setExecutor(new c_pmvc());

		Bukkit.getPluginManager().registerEvents(new l_click(), this);
	}

	public static main getmain() {
		return instance;
	}

}
