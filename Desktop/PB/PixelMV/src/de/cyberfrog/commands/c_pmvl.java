package de.cyberfrog.commands;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.cyberfrog.main.main;

/**
 * 
 * Created by Prisemut on 20.03.2018
 */
public class c_pmvl implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender cs, Command arg1, String arg2, String[] arg3) {
		Player p = (Player)cs;
		if(p.hasPermission("pmv.list")) {
			StringBuilder b = new StringBuilder();
			for(World w : Bukkit.getWorlds()) {
				b.append("�7"+w.getName()+"�4 ; ");
			}
			b.setLength(b.toString().length()-2);
			p.sendMessage(main.pr+"Folgende Welten wurden bis jetzt erstellt :");
			p.sendMessage(main.pr+b.toString());
		}else{
			p.sendMessage(main.pr+"Dazu hast du keine Rechte!");
		}
		return false;
	}

	
}
