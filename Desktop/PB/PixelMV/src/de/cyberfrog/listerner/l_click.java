package de.cyberfrog.listerner;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import de.cyberfrog.apis.ItemCreator;

public class l_click implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();

		if (e.getInventory().getName().equals("�dHelp")) {

			if (e.getCurrentItem().getType() == Material.GRASS) {
				p.closeInventory();

				Inventory i = Bukkit.createInventory(null, 9, "�4Types");
				i.setItem(0, ItemCreator.ci(Material.GRASS, "�aFlach (G, E, E, E, BR)"));
				i.setItem(1, ItemCreator.ci(Material.LEAVES, "�2Normal"));
				i.setItem(7, ItemCreator.ci(Material.DIAMOND_BLOCK, "�3Diamand (D, D, D, D, BR)"));
				i.setItem(8, ItemCreator.ci(Material.STONE, "�7Stein (S, S, S, S, BR"));
				p.openInventory(i);

			}
		} else if (e.getInventory().getName().equals("�cCommands")) {

			if (e.getCurrentItem().getType() == Material.ANVIL) {
				p.closeInventory();

				Inventory i = Bukkit.createInventory(null, 9, "�cCommands");
				i.setItem(0, ItemCreator.ci(Material.COBBLESTONE, "�a/pmvc <name> <Welt Typ>"));
				i.setItem(2, ItemCreator.ci(Material.COBBLESTONE, "�a/pmvl"));
				i.setItem(4, ItemCreator.ci(Material.COBBLESTONE, "�a/pmvt <Welt>"));
				i.setItem(6, ItemCreator.ci(Material.COBBLESTONE, "�a/pmvh"));
				i.setItem(8, ItemCreator.ci(Material.COBBLESTONE, "�a/pmvd <Welt>"));
				p.openInventory(i);
			}
		}
	}
}
