package de.cyberfrog.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.cyberfrog.main.main;

public class c_pmvd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command arg1, String arg2, String[] args) {
		Player p = (Player) cs;
		if (p.hasPermission("pmv.delete")) {
			if (args.length == 0) {
				if (Bukkit.getWorld(args[0]).getWorldFolder().exists()) {
					
					
					Bukkit.getWorld(args[0]).getWorldFolder().delete();
					Bukkit.broadcastMessage(main.pr+"�cDie Welt"+args[0]+" wurde von "+p.getName()+" gel�scht!");
					
					
				} else {
					p.sendMessage(main.pr+"Ung�ltige Welt! Mit /pmvl siehst du alle g�ltigen Welten!");
				}
			}else{
				p.sendMessage(main.pr+"/pmvd <Welt>");
			}
		}else{
			p.sendMessage(main.pr+"Dazu fehlen dir die Rechte!");
		}
		return false;
	}

}
