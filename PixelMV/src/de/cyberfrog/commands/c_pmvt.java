package de.cyberfrog.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.cyberfrog.main.main;

public class c_pmvt implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command arg1, String arg2, String[] a) {

		Player p = (Player) cs;
		if (p.hasPermission("pmv.teleport")) {
				p.sendMessage(main.pr + "Du wirst in die Welt " + a[0] + " teleportiert!");
				p.teleport(Bukkit.getWorld(a[0]).getSpawnLocation());		
		} else {
			p.sendMessage(main.pr + "Dazu hast du keine Rechte!");
		}
		return false;
	}

}
