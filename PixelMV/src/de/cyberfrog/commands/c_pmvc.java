package de.cyberfrog.commands;

import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.cyberfrog.main.main;

public class c_pmvc implements CommandExecutor {

	public String flach = "flach";
	public String leer = "leer";

	@Override
	public boolean onCommand(CommandSender cs, Command arg1, String arg2, String[] args) {
		Player p = (Player) cs;
		if (p.hasPermission("pmv.create")) {
			if (args.length == 2) {

				if (args[0].equalsIgnoreCase("Flach")) {

					p.sendMessage(main.pr+"Welt wird erstellt...");
					WorldCreator generator = new WorldCreator(args[1]);
					generator.environment(World.Environment.NORMAL);
					generator.type(WorldType.FLAT);
					main.getmain().getServer().createWorld(generator);
					p.sendMessage(main.pr + "Deine Welt mit dem Namen:�3 " + args[1] + ", wurde erstellt!");
					
					p.sendMessage(main.wi);
					
					
					
				} else if (args[0].equalsIgnoreCase("Normal")) {

					p.sendMessage(main.pr+"Welt wird erstellt...");
					WorldCreator generator = new WorldCreator(args[1]);
					generator.environment(World.Environment.NORMAL);
					generator.type(WorldType.NORMAL);
					main.getmain().getServer().createWorld(generator);
					p.sendMessage(main.pr + "Deine Welt mit dem Namen:�3 " + args[1] + ", wurde erstellt!");
					
					p.sendMessage(main.wi);
					
					
					
				}else{
					p.sendMessage(main.pr+"Alle Welt Typen findest du wenn du /pmvh ausf�hrst!");
				}

			} else {
				p.sendMessage(main.pr + "/pmvc <Typ> <Name>");
			}
		}
		return false;
	}

}
